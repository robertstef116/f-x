package tasks.services;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tasks.TestUtils;
import tasks.model.Task;

import java.util.*;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {

    private long getIterableLength(Iterable<?> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false).count();
    }

    @DisplayName("F02_TC01")
    @Test
    void test_incoming_with_begin_after_end() {
        Task t1 = new Task(1L, "description", "title", TestUtils.getDate(2020, 4, 1, 0, 0));
        Task t2 = new Task(2L, "description", "title", TestUtils.getDate(2020, 4, 3, 0, 0), TestUtils.getDate(2020, 4, 4, 0, 0), 24 * 60 * 60);
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>(Arrays.asList(t1, t2))));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 4, 2, 0, 0), TestUtils.getDate(2020, 4, 1, 0, 0));

        assertEquals(0, getIterableLength(tasks));
    }

    @DisplayName("F02_TC02")
    @Test
    void test_incoming_with_tasks_not_in_range() {
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>(Arrays.asList(
                new Task(1L, "description", "title", TestUtils.getDate(2020, 4, 1, 0, 0)),
                new Task(2L, "description", "title", TestUtils.getDate(2020, 4, 3, 0, 0), TestUtils.getDate(2020, 4, 4, 0, 0), 24 * 60 * 60),
                new Task(3L, "description", "title", TestUtils.getDate(2020, 4, 3, 6, 0), TestUtils.getDate(2020, 4, 4, 0, 0), 24 * 60 * 60),
                new Task(4L, "description", "title", TestUtils.getDate(2020, 4, 4, 0, 0), TestUtils.getDate(2020, 4, 5, 0, 0), 24 * 60 * 60),
                new Task(4L, "description", "title", TestUtils.getDate(2020, 4, 4, 6, 0), TestUtils.getDate(2020, 4, 6, 0, 0), 24 * 60 * 60)
        ))));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 3, 1, 0, 0), TestUtils.getDate(2020, 3, 5, 0, 0));

        assertEquals(0, getIterableLength(tasks));
    }

    @DisplayName("F02_TC03")
    @Test
    void test_incoming_with_task_in_range_finish_before_end() {
        Task t1 = new Task(1L, "description", "title", TestUtils.getDate(2020, 4, 3, 0, 0), TestUtils.getDate(2020, 4, 4, 0, 0), 24 * 60 * 60);
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>(Collections.singletonList(t1))));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 4, 3, 0, 0), TestUtils.getDate(2020, 4, 5, 0, 0));

        assertEquals(1, getIterableLength(tasks));
        assertEquals(t1, tasks.iterator().next());
    }

    @DisplayName("F02_TC04")
    @Test
    void test_incoming_with_no_task_execution_in_searched_range() {
        Task t1 = new Task(1L, "description", "title", TestUtils.getDate(2020, 4, 1, 12, 0), TestUtils.getDate(2020, 4, 5, 14, 0), 2 * 24 * 60 * 60);
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>(Collections.singletonList(t1))));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 4, 4, 12, 0), TestUtils.getDate(2020, 4, 5, 11, 0));

        assertEquals(0, getIterableLength(tasks));
    }

    @DisplayName("F02_TC05")
    @Test
    void test_incoming_with_task_match_search_dates() {
        Task t1 = new Task(1L, "description", "title", TestUtils.getDate(2020, 4, 1, 0, 0), TestUtils.getDate(2020, 4, 2, 0, 0), 24 * 60 * 60);
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>(Collections.singletonList(t1))));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 4, 1, 0, 0), TestUtils.getDate(2020, 4, 2, 0, 0));

        assertEquals(1, getIterableLength(tasks));
        assertEquals(t1, tasks.iterator().next());
    }

    @DisplayName("F02_TC06")
    @Test
    void test_incoming_with_no_tasks() {
        TasksOperations to = new TasksOperations(FXCollections.observableList(new ArrayList<>()));

        Iterable<Task> tasks = to.incoming(TestUtils.getDate(2020, 4, 1, 0, 0), TestUtils.getDate(2020, 4, 2, 0, 0));

        assertEquals(0, getIterableLength(tasks));
    }
}