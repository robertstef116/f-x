package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.TestUtils;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationArrayTaskListTest {

    @Mock
    private Task task;

    private Date taskDate = TestUtils.getDate(2020, 4,4,10,0);

    private ArrayTaskList arrayTaskList;

    private TasksService tasksService;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(task.nextTimeAfter(Mockito.any(Date.class))).thenReturn(taskDate);
        arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void test_filter_task_when_task_in_range() {
        Date start = TestUtils.getDate(2020,3,3,10,0);
        Date end = TestUtils.getDate(2020,5,5,10,0);
        List<Task> filteredTaskList = new ArrayList<>();
        Iterable<Task> filtered = tasksService.filterTasks(start, end);
        filtered.iterator().forEachRemaining(filteredTaskList::add);
        assertEquals(1, filteredTaskList.size());
        assertEquals(task, filteredTaskList.get(0));
        Mockito.verify(task, Mockito.times(1)).nextTimeAfter(start);
    }

    @Test
    void test_filter_task_when_task_not_in_range() {
        Date start = TestUtils.getDate(2020,3,3,10,0);
        Date end = TestUtils.getDate(2020,3,5,10,0);
        List<Task> filteredTaskList = new ArrayList<>();
        Iterable<Task> filtered = tasksService.filterTasks(start, end);
        filtered.iterator().forEachRemaining(filteredTaskList::add);
        assertEquals(0, filteredTaskList.size());
        Mockito.verify(task, Mockito.times(1)).nextTimeAfter(start);
    }

}
