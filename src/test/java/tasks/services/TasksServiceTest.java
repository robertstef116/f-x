package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import tasks.TestUtils;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceTest {

    @Mock
    private ArrayTaskList tasks;

    @Mock
    private Task task;

    @InjectMocks
    private TasksService tasksService;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(task.getRepeatInterval()).thenReturn(7777);
        Mockito.when(tasks.getAll()).thenReturn(Collections.singletonList(task));
    }

    @Test
    void test_get_interval_in_hours() {
        assertEquals("02:09", tasksService.getIntervalInHours(task));
        Mockito.verify(task, Mockito.times(1)).getRepeatInterval();
    }

    @Test
    void test_get_observable_list() {
        assertEquals(FXCollections.observableList(Collections.singletonList(task)), tasksService.getObservableList());
        Mockito.verify(tasks, Mockito.times(1)).getAll();
    }
}