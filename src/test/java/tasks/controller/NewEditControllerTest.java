package tasks.controller;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class NewEditControllerTest {

    private static NewEditController ctrl;
    private static Method method;

    @BeforeAll
    static void beforeAll()  throws NoSuchMethodException {
        ctrl = new NewEditController();
        ctrl.setTasksList(FXCollections.observableArrayList());
        Class c = NewEditController.class;
        method= c.getDeclaredMethod("addTask", String.class, String.class, Date.class, Date.class, int.class, boolean.class);
        method.setAccessible(true);
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    private static boolean addTask(String title, String description, Date startDate, Date endDate, int interval, boolean active) {
        try {
            return (boolean) method.invoke(ctrl, title, description, startDate, endDate, interval, active);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Tag("ECP")
    @DisplayName("TC6_ECP")
    void testAddTask1() {
        assertTrue(addTask("Go shopping", "asd", new Date(), null,  0, true));
    }

    @Test
    @Tag("ECP")
    @DisplayName("TC1_ECP")
    void testAddTask9() {
        assertTrue(addTask("Go shopping", "asd", new Date(), null,  0, false));
    }

    @Test
    @Tag("ECP")
    @DisplayName("TC2_ECP")
    void testAddTask2() {
        assertFalse(addTask("Go", "asd", new Date(), null, 0, true));
    }

    @Test
    @Tag("ECP")
    @DisplayName("TC7_ECP")
    void testAddTask10() {
        assertFalse(addTask("Go", "asd", new Date(), null, 0, false));
    }

    @Tag("ECP")
    @DisplayName("TC3-8_ECP")
    @Timeout(3)
    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    void testAddTask3(Boolean active) {
        assertFalse(addTask("123456789012345678901", "asd", new Date(), null, 0, active));
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC1_BVA")
    void testAddTask4() {
        assertTrue(addTask("12345", "asd", new Date(), null, 0, true));
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC2_BVA")
    void testAddTask5() {
        assertFalse(addTask("1234", "asd", new Date(), null, 0, true));
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC3_BVA")
    void testAddTask6() {
        assertTrue(addTask("123456", "asd", new Date(), null, 0, true));
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC4_BVA")
    void testAddTask7() {
        assertTrue(addTask("12345678901234567890", "asd", new Date(), null, 0, true));
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC5_BVA")
    void testAddTask8() {
        assertTrue(addTask("1234567890123456789", "asd", new Date(), null, 0, true));
    }
}