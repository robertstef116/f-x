package tasks.repository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import tasks.TestUtils;
import tasks.model.Task;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ArrayTaskListTest {
    private ArrayTaskList atl;

    @Mock
    private Task mockTask;

    @Spy
    private Task spyTask = new Task(123L, "description", "title", TestUtils.getDate(2020, 2,2,12,0));

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        atl = new ArrayTaskList();
    }

    @Test
    void test_add_task() {
        assertEquals(0, atl.getAll().size());
        assertEquals(0, atl.size());
        atl.add(mockTask);
        assertEquals(1, atl.getAll().size());
        assertEquals(1, atl.size());
    }

    @Test
    void test_remove_task() {
        assertEquals(0, atl.getAll().size());
        assertEquals(0, atl.size());
        atl.add(spyTask);
        assertTrue(atl.remove(spyTask));
        verify(spyTask, times(2)).getId();
    }

    @Test
    void test_remove_task_when_no_task_added() {
        assertEquals(0, atl.getAll().size());
        assertEquals(0, atl.size());
        assertFalse(atl.remove(spyTask));
        verify(spyTask, times(0)).getId();
    }
}