package tasks.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import tasks.model.Task;
import tasks.services.DateService;
import tasks.services.TaskFileWriter;
import tasks.services.TasksService;
import tasks.utils.TimeUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;


public class NewEditController {

    private static Button clickedButton;

    private static final Logger log = Logger.getLogger(NewEditController.class.getName());
    private TaskFileWriter taskIO;

    public static void setClickedButton(Button clickedButton) {
        NewEditController.clickedButton = clickedButton;
    }

    public static void setCurrentStage(Stage currentStage) {
        NewEditController.currentStage = currentStage;
    }

    private static Stage currentStage;

    private Task currentTask;
    private ObservableList<Task> tasksList;
    private TasksService service;
    private DateService dateService;


    private boolean incorrectInputMade;
    @FXML
    private TextField fieldTitle;
    @FXML
    private TextField fieldDescription;
    @FXML
    private DatePicker datePickerStart;
    @FXML
    private TextField txtFieldTimeStart;
    @FXML
    private DatePicker datePickerEnd;
    @FXML
    private TextField txtFieldTimeEnd;
    @FXML
    private TextField fieldInterval;
    @FXML
    private CheckBox checkBoxActive;
    @FXML
    private CheckBox checkBoxRepeated;

    private static final String DEFAULT_START_TIME = "8:00";
    private static final String DEFAULT_END_TIME = "10:00";
    private static final String DEFAULT_INTERVAL_TIME = "0:30";

    public void setTasksList(ObservableList<Task> tasksList) {
        this.tasksList = tasksList;
    }

    public void setService(TasksService service) {
        this.service = service;
        this.dateService = new DateService();
    }

    public void setCurrentTask(Task task) {
        this.currentTask = task;
        if (clickedButton.getId().equals("btnNew")) initNewWindow();
        if (clickedButton.getId().equals("btnEdit")) initEditWindow();
    }

    @FXML
    public void initialize() {
        log.info("new/edit window initializing");
    }

    private void initNewWindow() {
        currentStage.setTitle("New Task");
        datePickerStart.setValue(LocalDate.now());
        txtFieldTimeStart.setText(DEFAULT_START_TIME);
    }

    private void initEditWindow() {
        currentStage.setTitle("Edit Task");
        fieldTitle.setText(currentTask.getTitle());
        datePickerStart.setValue(DateService.getLocalDateValueFromDate(currentTask.getStartTime()));
        txtFieldTimeStart.setText(dateService.getTimeOfTheDayFromDate(currentTask.getStartTime()));

        if (currentTask.isRepeated()) {
            checkBoxRepeated.setSelected(true);
            changeStateRepeatedTaskControllers(false);
            datePickerEnd.setValue(DateService.getLocalDateValueFromDate(currentTask.getEndTime()));
            fieldInterval.setText(service.getIntervalInHours(currentTask));
            txtFieldTimeEnd.setText(dateService.getTimeOfTheDayFromDate(currentTask.getEndTime()));
        }
        if (currentTask.isActive()) {
            checkBoxActive.setSelected(true);

        }
    }

    @FXML
    public void switchRepeatedCheckbox(ActionEvent checkBoxEvent) {
        CheckBox source = (CheckBox) checkBoxEvent.getSource();
        if (source.isSelected()) {
            changeStateRepeatedTaskControllers(false);
        } else if (!source.isSelected()) {
            changeStateRepeatedTaskControllers(true);
        }
    }

    private void changeStateRepeatedTaskControllers(boolean newState) {
        datePickerEnd.setDisable(newState);
        fieldInterval.setDisable(newState);
        txtFieldTimeEnd.setDisable(newState);

        datePickerEnd.setValue(LocalDate.now());
        txtFieldTimeEnd.setText(DEFAULT_END_TIME);
        fieldInterval.setText(DEFAULT_INTERVAL_TIME);
    }

    @FXML
    public void saveChanges() {
        if (incorrectInputMade) return;
        if (currentTask == null) {//no task was chosen -> add button was pressed
            String newTitle = fieldTitle.getText();
            String newDescription = fieldDescription.getText();
            Date startDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerStart.getValue());//ONLY date!!without time
            Date newStartDate = dateService.getDateMergedWithTime(txtFieldTimeStart.getText(), startDateWithNoTime);
            boolean added;
            if (checkBoxRepeated.isSelected()) {
                Date endDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerEnd.getValue());
                Date newEndDate = dateService.getDateMergedWithTime(txtFieldTimeEnd.getText(), endDateWithNoTime);
                int newInterval = TimeUtils.parseFromStringToSeconds(fieldInterval.getText());
                added = addTask(newTitle, newDescription, newStartDate, newEndDate, newInterval, checkBoxActive.isSelected());
            } else {
                added = addTask(newTitle, newDescription, newStartDate, null, 0, checkBoxActive.isSelected());
            }
            if (added) {
                new Alert(Alert.AlertType.CONFIRMATION, "Task added!").showAndWait();
            } else {
                new Alert(Alert.AlertType.ERROR, "Task not added!").showAndWait();
            }
        } else {
            Task collectedFieldsTask = collectFieldsData();
            for (int i = 0; i < tasksList.size(); i++) {
                if (currentTask.equals(tasksList.get(i))) {
                    tasksList.set(i, collectedFieldsTask);
                }
            }
            currentTask = null;
        }
        taskIO.rewriteFile(tasksList);
        Controller.editNewStage.close();
    }

    private boolean addTask(String title, String description, Date startDate, Date endDate, int interval, boolean active) {
        if (title.length() >= 5 && title.length() <= 20) {
            Task result = null;
            if (endDate == null) {
                result = new Task(new Date().getTime(), title, description, startDate);
            } else {
                result = new Task(new Date().getTime(), title, description, startDate, endDate, interval);
            }
            result.setActive(active);
            tasksList.add(result);
            return true;
        }
        return false;
    }

    @FXML
    public void closeDialogWindow() {
        Controller.editNewStage.close();
    }

    private Task collectFieldsData() {
        incorrectInputMade = false;
        Task result = null;
        try {
            result = makeTask();
        } catch (RuntimeException e) {
            incorrectInputMade = true;
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("/fxml/field-validator.fxml"));
                stage.setScene(new Scene(root, 350, 150));
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException ioe) {
                log.error("error loading field-validator.fxml");
            }
        }
        return result;
    }

    private Task makeTask() {
        Task result;
        String newTitle = fieldTitle.getText();
        String newDescription = fieldDescription.getText();
        Date startDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerStart.getValue());//ONLY date!!without time
        Date newStartDate = dateService.getDateMergedWithTime(txtFieldTimeStart.getText(), startDateWithNoTime);
        if (checkBoxRepeated.isSelected()) {
            Date endDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerEnd.getValue());
            Date newEndDate = dateService.getDateMergedWithTime(txtFieldTimeEnd.getText(), endDateWithNoTime);
            int newInterval = TimeUtils.parseFromStringToSeconds(fieldInterval.getText());
            if (newStartDate.after(newEndDate)) throw new IllegalArgumentException("Start date should be before end");
            result = new Task(new Date().getTime(), newDescription, newTitle, newStartDate, newEndDate, newInterval);
        } else {
            result = new Task(new Date().getTime(), newDescription, newTitle, newStartDate);
        }
        boolean isActive = checkBoxActive.isSelected();
        result.setActive(isActive);
        log.debug(result);
        return result;
    }


    public void setTaskIO(TaskFileWriter taskIO) {
        this.taskIO = taskIO;
    }
}
