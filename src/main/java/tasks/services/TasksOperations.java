package tasks.services;

import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.model.Task;

import java.util.*;

public class TasksOperations {
    private static final Logger log = Logger.getLogger(TasksOperations.class.getName());
    private List<Task> tasks;

    public TasksOperations(ObservableList<Task> tasksList) {
        tasks = new ArrayList<>();
        tasks.addAll(tasksList);
    }

    public Iterable<Task> incoming(Date start, Date end) {
        Date nextTime;
        ArrayList<Task> incomingTasks = new ArrayList<>();
        for (Task t : tasks) {
            if (start.after(end) || start.equals(end)) {
                nextTime= null;
            } else {
                nextTime = t.nextTimeAfter(start);
            }
            if (nextTime != null) {
                if (nextTime.before(end)) {
                    incomingTasks.add(t);
                }
                if (nextTime.equals(end)) {
                    incomingTasks.add(t);
                }
            }
        }
        return incomingTasks;
    }
}
