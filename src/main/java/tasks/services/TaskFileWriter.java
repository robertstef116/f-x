package tasks.services;

import javafx.collections.ObservableList;
import tasks.model.Task;
import tasks.repository.TaskList;

import java.io.File;

public interface TaskFileWriter {
    void read(TaskList tasks, File file);
    void write(TaskList tasks, File file);
    void rewriteFile(ObservableList<Task> tasksList);
}
