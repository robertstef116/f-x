package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasks.repository.ArrayTaskList;
import tasks.model.Task;
import tasks.utils.TimeUtils;

import java.util.Date;

public class TasksService {

    private ArrayTaskList tasks;

    public TasksService(ArrayTaskList tasks){
        this.tasks = tasks;
    }


    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }

    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return TimeUtils.formTimeUnit(hours) + ":" + TimeUtils.formTimeUnit(minutes);//hh:MM
    }



    public Iterable<Task> filterTasks(Date start, Date end){
        TasksOperations tasksOps = new TasksOperations(getObservableList());
        return tasksOps.incoming(start,end);
    }
}
