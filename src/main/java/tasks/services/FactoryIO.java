package tasks.services;

import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.model.Task;
import tasks.repository.LinkedTaskList;
import tasks.repository.TaskList;
import tasks.utils.TimeUtils;
import tasks.view.Main;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class FactoryIO {
    private FactoryIO(){}
    public enum IO {
        BINARY,
        TEXT
    }
    public static TaskFileWriter getIOMethod(IO io) {
        switch (io) {
            case BINARY:
                return new BinaryTaskFileWriter();
            case TEXT:
                return new TextTaskFileWriter();
            default:
                return null;
        }
    }
}

abstract class AbstractTaskFileWriter implements TaskFileWriter {

    @Override
    public void rewriteFile(ObservableList<Task> tasksList) {
        LinkedTaskList taskList = new LinkedTaskList();
        for (Task t : tasksList){
            taskList.add(t);
        }
        write(taskList, Main.savedTasksFile);
    }
}

class BinaryTaskFileWriter extends AbstractTaskFileWriter {
    private static final Logger log = Logger.getLogger(BinaryTaskFileWriter.class.getName());

    @Override
    public void read(TaskList tasks, File file) {
        try (FileInputStream fis = new FileInputStream(file)){
            read(tasks, fis);
        }
        catch (IOException e){
            log.error("IO exception reading or writing file");
        }
    }

    @Override
    public void write(TaskList tasks, File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            write(tasks,fos);
        }
        catch (IOException e){
            log.error(e.getMessage());
        }
    }

    private static void write(TaskList tasks, OutputStream out) throws IOException {
        try (DataOutputStream dataOutputStream = new DataOutputStream(out)) {
            dataOutputStream.writeInt(tasks.size());
            for (Task t : tasks) {
                dataOutputStream.writeLong(t.getId());
                dataOutputStream.writeInt(t.getDescription().length());
                dataOutputStream.writeUTF(t.getDescription());
                dataOutputStream.writeInt(t.getTitle().length());
                dataOutputStream.writeUTF(t.getTitle());
                dataOutputStream.writeBoolean(t.isActive());
                dataOutputStream.writeInt(t.getRepeatInterval());
                if (t.isRepeated()) {
                    dataOutputStream.writeLong(t.getStartTime().getTime());
                    dataOutputStream.writeLong(t.getEndTime().getTime());
                } else {
                    dataOutputStream.writeLong(t.getTime().getTime());
                }
            }
        }
    }
    private static void read(TaskList tasks, InputStream in)throws IOException {
        try (DataInputStream dataInputStream = new DataInputStream(in)) {
            int listLength = dataInputStream.readInt();
            for (int i = 0; i < listLength; i++) {
                Long id = dataInputStream.readLong();
                dataInputStream.readInt();
                String description = dataInputStream.readUTF();
                dataInputStream.readInt();
                String title = dataInputStream.readUTF();
                boolean isActive = dataInputStream.readBoolean();
                int interval = dataInputStream.readInt();
                Date startTime = new Date(dataInputStream.readLong());
                Task taskToAdd;
                if (interval > 0) {
                    Date endTime = new Date(dataInputStream.readLong());
                    taskToAdd = new Task(id, description, title, startTime, endTime, interval);
                } else {
                    taskToAdd = new Task(id, description, title, startTime);
                }
                taskToAdd.setActive(isActive);
                tasks.add(taskToAdd);
            }
        }
    }
}

class TextTaskFileWriter extends AbstractTaskFileWriter  {
    private static final Logger log = Logger.getLogger(TextTaskFileWriter.class.getName());
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS]");

    @Override
    public void read(TaskList tasks, File file) {
        try (FileReader fileReader = new FileReader(file)) {
            read(tasks, fileReader);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void write(TaskList tasks, File file) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            write(tasks, fileWriter);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public static void write(TaskList tasks, Writer out) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(out);
        Task lastTask = tasks.getTask(tasks.size()-1);
        for (Task t : tasks){
            bufferedWriter.write(getFormattedTask(t));
            bufferedWriter.write(t.equals(lastTask) ? ';' : '.');
            bufferedWriter.newLine();
        }
        bufferedWriter.close();

    }

    public static void read(TaskList tasks, Reader in)  throws IOException {
        BufferedReader reader = new BufferedReader(in);
        String line;
        Task t;
        while ((line = reader.readLine()) != null){
            t = getTaskFromString(line);
            tasks.add(t);
        }
        reader.close();

    }

    private static Task getTaskFromString (String line){
        boolean isRepeated = line.contains("from");//if contains - means repeated
        boolean isActive = !line.contains("inactive");//if doesnt have inactive - means active
        //Task(String title, Date time)   Task(String title, Date start, Date end, int interval)
        Task result;
        Long id = getIdFromText(line);
        String description = getDescriptionFromText(line);
        String title = getTitleFromText(line);
        if (isRepeated){
            Date startTime = getDateFromText(line, true);
            Date endTime = getDateFromText(line, false);
            int interval = getIntervalFromText(line);
            result = new Task(id, description, title, startTime, endTime, interval);
        }
        else {
            Date startTime = getDateFromText(line, true);
            result = new Task(id, description, title, startTime);
        }
        result.setActive(isActive);
        return result;
    }

    private static String getDescriptionFromText(String line) {
        int start = line.indexOf(' ')+1;
        int end = line.lastIndexOf('|');
        return line.substring(start, end);
    }

    private static Long getIdFromText(String line) {
        int end = line.indexOf(' ');
        return Long.parseLong(line.substring(0, end));
    }

    //
    private static int getIntervalFromText(String line){
        int days;
        int hours;
        int minutes;
        int seconds;
        int start = line.lastIndexOf('[');
        int end = line.lastIndexOf(']');
        String trimmed = line.substring(start+1, end);
        days = trimmed.contains("day") ? 1 : 0;
        hours = trimmed.contains("hour") ? 1 : 0;
        minutes = trimmed.contains("minute") ? 1 : 0;
        seconds = trimmed.contains("second") ? 1 : 0;

        int[] timeEntities = new int[]{days, hours, minutes, seconds};
        int i = 0;
        int j = timeEntities.length-1;
        while (i != 1 && j != 1) {
            if (timeEntities[i] == 0) i++;
            if (timeEntities[j] == 0) j--;
        }

        String[] numAndTextValues = trimmed.split(" ");
        for (int k = 0 ; k < numAndTextValues.length; k+=2){
            timeEntities[i] = Integer.parseInt(numAndTextValues[k]);
            i++;
        }

        int result = 0;
        for (int p = 0; p < timeEntities.length; p++){
            if (timeEntities[p] != 0 && p == 0){
                result+= TimeUtils.SECONDS_IN_DAY *timeEntities[p];
            }
            if (timeEntities[p] != 0 && p == 1){
                result+= TimeUtils.SECONDS_IN_HOUR *timeEntities[p];
            }
            if (timeEntities[p] != 0 && p == 2){
                result+= TimeUtils.SECONDS_IN_MIN *timeEntities[p];
            }
            if (timeEntities[p] != 0 && p == 3){
                result+=timeEntities[p];
            }
        }
        return result;
    }

    private static Date getDateFromText (String line, boolean isStartTime) {
        Date date = null;
        String trimmedDate; //date trimmed from whole string
        int start;
        int end;

        if (isStartTime){
            start = line.indexOf('[');
            end = line.indexOf(']');
        }
        else {
            int firstRightBracket = line.indexOf(']');
            start = line.indexOf('[', firstRightBracket+1);
            end = line.indexOf(']', firstRightBracket+1);
        }
        trimmedDate = line.substring(start, end+1);
        try {
            date = simpleDateFormat.parse(trimmedDate);
        }
        catch (ParseException e){
            log.error("date parse exception");
        }
        return date;
    }

    private static String getTitleFromText(String line){
        int start = line.indexOf('"')+1;
        int end = line.lastIndexOf('"');
        String result = line.substring(start, end);
        result = result.replace("\"\"", "\"");
        return result;
    }

    private static String getFormattedTask(Task task){
        StringBuilder result = new StringBuilder();
        result.append(task.getId());
        result.append(" ");
        result.append(task.getDescription());
        result.append("|");
        String title = task.getTitle();
        if (title.contains("\"")) title = title.replace("\"","\"\"");
        result.append("\"").append(title).append("\"");

        if (task.isRepeated()){
            result.append(" from ");
            result.append(simpleDateFormat.format(task.getStartTime()));
            result.append(" to ");
            result.append(simpleDateFormat.format(task.getEndTime()));
            result.append(" every ").append("[");
            result.append(TimeUtils.getFormattedInterval(task.getRepeatInterval()));
            result.append("]");
        }
        else {
            result.append(" at ");
            result.append(simpleDateFormat.format(task.getStartTime()));
        }
        if (!task.isActive()) result.append(" inactive");
        return result.toString().trim();
    }
}
