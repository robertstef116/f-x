package tasks.view;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import tasks.controller.Controller;
import tasks.services.*;
import tasks.repository.ArrayTaskList;

import java.io.File;
import java.io.IOException;
import java.util.Objects;


public class Main extends Application {
    private static final int DEFAULT_WIDTH = 820;
    private static final int DEFAULT_HEIGHT = 520;

    private static final Logger log = Logger.getLogger(Main.class.getName());

    private ArrayTaskList savedTasksList = new ArrayTaskList();

    private static ClassLoader classLoader = Main.class.getClassLoader();
    public static final File savedTasksFile = new File(Objects.requireNonNull(classLoader.getResource("data/tasks.txt")).getFile());

    private Notificator notificator;

    private TasksService service = new TasksService(savedTasksList);

    private TaskFileWriter taskIO = FactoryIO.getIOMethod(FactoryIO.IO.BINARY);

    @Override
    public void start(Stage primaryStage) {
        log.info("saved data reading");
        if (savedTasksFile.length() != 0) {
            taskIO.read(savedTasksList, savedTasksFile);
        }
        try {
            log.info("application start");
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
            Parent root = loader.load();
            Controller ctrl = loader.getController();
            service = new TasksService(savedTasksList);

            ctrl.setService(service);
            ctrl.setTaskIO(taskIO);

            primaryStage.setTitle("Task Manager");
            primaryStage.setScene(new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT));
            primaryStage.setMinWidth(DEFAULT_WIDTH);
            primaryStage.setMinHeight(DEFAULT_HEIGHT);
            primaryStage.show();
        } catch (IOException e) {
            log.error("error reading main.fxml");
        }
        primaryStage.setOnCloseRequest(we -> {
            notificator.disable();
            try {
                notificator.join();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
            System.exit(0);
        });
        notificator = new Notificator(FXCollections.observableArrayList(service.getObservableList()));
        notificator.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
